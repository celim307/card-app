import React, { PropTypes, Component } from 'react';

import './Card.css';

class Card extends Component {

  constructor(props) {
    super();
    this.state = {
      show: props.show
    }
  }

  handleClick(e) {
    this.setState({show: true});
    setTimeout(() => { 
      this.props.clickCardHandler(this.props.cardObj);
      this.setState({show: false}); 
    }, 1000);
  }


  render() {
    return (
      <div
        className="Card"
        onClick={this.handleClick.bind(this)}>
        <span>{(this.props.show || this.state.show) ? this.props.cardObj.value : '?'}</span>
      </div>
    );
  }
}

Card.propTypes = {
  cardObj: PropTypes.shape({
    id: PropTypes.string.isRequired,
    value: PropTypes.number.isRequired
  }),
  show: PropTypes.bool.isRequired,
  clickCardHandler: PropTypes.func.isRequired
};

export default Card;
