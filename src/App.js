import React, { Component } from 'react';

import Card from './Card'
import './App.css';

const noCard = {
    id: null,
    value: null
  }

class App extends Component {

  constructor() {
    super();
    this.initialCards = this.shuffleArray(this.generateCardPairs(12));
    this.state = {
      unmatchedCards: this.initialCards,
      selectedCard: noCard
    }
  }

  generateCardPairs(numPairs) {
    let currentList = [];
    for (let i = 0; i < numPairs; i+= 1) {
      currentList = currentList.concat(this.createCard(i));
    }
    return currentList;
  }

  shuffleArray(array) {
    let i = array.length - 1;
    for (; i > 0; i--) {
      const j = Math.floor(Math.random() * (i + 1));
      const temp = array[i];
      array[i] = array[j];
      array[j] = temp;
    }
    return array;
  }

  createCard(value) {
    return [{
      id: `${value}a`,
      value
    },{
      id : `${value}b`,
      value
    }]
  }


  clickCardHandler({id, value}) {
    if (!this.state.selectedCard.id) {
      this.setState({selectedCard: { id, value}});
    } else {
      if (this.state.selectedCard.id !== id && this.state.selectedCard.value === value) {
        const unmatchedCards = this.state.unmatchedCards.filter(element => {
          return element.value !== value;
        });
        this.setState({unmatchedCards});
      }
      this.setState({selectedCard: noCard});
    }
  }

  render() {
    return (
      <div className="App">
        <div className="App-header">
          <h2>Card matcher 2000</h2>
        </div>
        <div className="Card-Container">
          {
            this.state.unmatchedCards.map(element => {
              return (
                <Card
                key={element.id}
                cardObj={element}
                show={element.id === this.state.selectedCard.id}
                clickCardHandler={this.clickCardHandler.bind(this)}
                />
              );
            })
          }
          {
            this.state.unmatchedCards.length === 0 && <span>You won!</span>
          }
        </div>
      </div>
    );
  }
}

export default App;
